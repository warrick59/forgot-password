<?php
include '../core/BDD.php';
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Changement du mot de passe</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  <style media="screen">
    body {
      background: #4db6ac;
    }
  </style>
</head>

<body>
  <?php  $cle =  addslashes($_GET['key']); ?>

  <div class="container">
    <div class="row">
      <div class="card col s12 m6 offset-m3">
        <form action="update-forgot-password.php?key=<?php echo $cle; ?>" method="post" class="form-signin">
          <div class="card-content">
            <span class="card-title">Nouveau mot passe</span>
            <input type='password' class='form-control' placeholder='Nouveau mot de passe' name='password' required>
          </div>
          <div class="card-action center">
            <input class="waves-effect waves-light btn" type="submit" name="modifier">
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php

  if(isset($_POST['modifier'])) {
    $password = addslashes(sha1($_POST["password"]));
    $query1 = "SELECT * FROM users WHERE cle = '$cle'";
    $result1 = mysqli_query($handle,$query1);
    $data=mysqli_fetch_array($result1);
    if($data["cle"]==$cle) {
      echo "$cle";
      $query = "UPDATE users SET cle = '0', password = '$password' WHERE cle = '$cle'";
      $result = mysqli_query($handle,$query);
      echo "$cle";
      echo "Votre mot de passe a bien été modifié.";
       header("Refresh: 6;url=89.95.28.201/password"); 
    }
    else {
      echo "Le champ est vide, veuillez entrer un mot de passe.";
    }
  }
  echo "$password";
  echo "$cle";
  ?>
</body>
</html>
