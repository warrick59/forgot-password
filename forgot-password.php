<?php
include 'core/BDD.php';
include 'include/send_mail.php';
?>
<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Récupération du mot de passe</title>
  <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/css/materialize.min.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.7/js/materialize.min.js"></script>
  <style media="screen">
    body {
      background: #4db6ac;
    }
  </style>
</head>
<body>

  <div class="container">
    <div class="row">
      <div class="card col s12 m6 offset-m3">
        <form action="forgot-password.php" method="post" class="form-signin">
          <div class="card-content">
            <span class="card-title">Récupération du mot de passe</span>
            <input type="text" class="form-control" placeholder="email" name="email" required="">
          </div>
          <div class="card-action center">
            <input class="waves-effect waves-light btn" type="submit" name="modifier">
          </div>
        </form>
      </div>
    </div>
  </div>

  <?php
  $caracteres = array("a", "b", "c", "d", "e", "f", "g", "h", "i", "j",  0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
  $caracteres_aleatoires = array_rand($caracteres, 8);
  $cle = "";

  foreach($caracteres_aleatoires as $i)
  {
    $active = 0;
    $cle .= $caracteres[$i];
  }




  if(isset($_POST["modifier"])) {
    $email =  addslashes($_POST['email']);
    $query1 = "SELECT * FROM users WHERE email = '$email'";
    $result1 = mysqli_query($handle,$query1);
    $data=mysqli_fetch_array($result1);
    if($data["email"]==$email) {
      $query = "UPDATE users SET cle = '$cle' WHERE email = '$email'";
      $result = mysqli_query($handle,$query);
      echo "Un email vient de vous être envoyé pour réinitialiser votre mot de passe.";
      send_mail($email,$cle);
    }
    else {
      echo "Cet email n'existe pas ou vous avez fait une erreur de frappe.";
    }
  }

  ?>

</body>
</html>
